package com.eurovision.cities.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eurovision.cities.entity.City;
import com.eurovision.cities.service.CityService;

@RestController
@RequestMapping("/api/cities")
public class CityController {
    
	@Autowired
    CityService cityService;

    @GetMapping(value = "/queryByPage", produces =  MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Page<City>> getCities(@RequestParam("page") int page, @RequestParam("size") int size) {
      return new ResponseEntity<>(cityService.getCities(page, size), HttpStatus.OK);
    }
    
    @GetMapping(value = "/sequence", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Integer>> getCitiesLongestSequence(){
    	return new ResponseEntity<>(cityService.getBiggestOrdererSequenceByName(), HttpStatus.OK);
    }
}
