package com.eurovision.cities.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.eurovision.cities.entity.City;

@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Integer>{
	
	
}
