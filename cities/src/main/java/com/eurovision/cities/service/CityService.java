package com.eurovision.cities.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.eurovision.cities.entity.City;

public interface CityService {

	public Page<City> getCities(int page, int size);
	public List<Integer> getBiggestOrdererSequenceByName();
}
