package com.eurovision.cities.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.eurovision.cities.entity.City;
import com.eurovision.cities.repository.CityRepository;

/**
 * Class CityServiceImpl to manage all operations with Cities
 */
@Service
public class CityServiceImpl implements CityService{

	/** The city repository. */
	@Autowired
	CityRepository cityRepository;
	
	/**
	 * Gets the cities by page
	 *
	 * @param page the page
	 * @param size the size
	 * @return the cities
	 */
	public Page<City> getCities(int page, int size) {
		PageRequest pageRequest = PageRequest.of(page, size);
		return cityRepository.findAll(pageRequest);
	}
	
	/**
	 * Gets the biggest orderer ids sequence of cities by name.
	 *
	 * @return the biggest orderer sequence of ids by name
	 */
	public List<Integer> getBiggestOrdererSequenceByName(){
		Sort sort = Sort.by(Direction.ASC, "name");
		Page<City> cities;
		int page = 0;
		int size = 30;
		
		PageRequest pageRequest = PageRequest.of(page, size, sort);
		cities = cityRepository.findAll(pageRequest);
		
		//The root where all nodes will be pending
		TreeId root = new TreeId(-1);
		
		while(!cities.getContent().isEmpty()) {
			
			//Inserts each city
			for(City city: cities.getContent()) {
				root.insert(city.getId());
			}
			
			page++;
			pageRequest = PageRequest.of(page, size, sort);
			cities = cityRepository.findAll(pageRequest);
			
			//Clean tree to reduce number of operations each time we get a new page
			root.cleanTree();
		}
		
		return root.getLongestList();
	}
	
	
}
