package com.eurovision.cities.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeId {
	private Integer num;
	private TreeId parent = null;
	private Integer depth = 0;
	private List<TreeId> childs = null;
	private Integer level = 0;
	
	TreeId(Integer num, TreeId parent){
		this.num = num;
		this.parent = parent;
		this.level = parent.level+1;
	}
	
	TreeId(Integer num){
		this.num = num;
	}
	
	void insert(Integer number) {
		TreeId treeToInsert = getSmallestByDepth(number);
		treeToInsert.insertChild(number);
	}

	TreeId getSmallestByDepth(Integer search) {
		TreeId current = this;
		
		if(childs != null) {
			for(TreeId tree: childs) {
				if(tree.getData() < search && current.level <= tree.level) {
					current = tree.getSmallestByDepth(search);
				}
			}
		}
		
		if(current != null) {
			return current;
		} else if(this.num < search){
			return this;
		} else {
			return null;
		}
	}
	
	Integer getData() {
		return num;
	}
	
	Integer getDepth() {
		return depth;
	}
	
	private void insertChild(Integer num) {
		TreeId node = new TreeId(num, this);
		if(childs == null)
			childs = new LinkedList<>();
		
		childs.add(node);
		
		if(depth == 0)
			updateDepth();
	}
	
	private void updateDepth() {
		TreeId current = this;
		current.depth++;
		while(current != null && current.parent != null) {
			if(current.depth >= current.parent.depth) {
				current = current.parent;
				current.depth++;
			} else {
				current = null;
			}
		}
	}

	@Override
	public String toString() {
		return "TreeId [num=" + num + ", depth=" + depth + ", level="
				+ level + "]";
	}
	
	public List<Integer> getLongestList(){
		TreeId current = this;
		List<Integer> longest = new ArrayList<>();
		while(current.childs != null && !current.childs.isEmpty()) {
			TreeId next = current.childs.get(0);
			for(TreeId theChild: current.childs) {
				if(theChild.depth >= next.depth && theChild.num < next.num)
					next = theChild;
			}
			longest.add(next.getData());
			current = next;
		}
		return longest;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreeId other = (TreeId) obj;
		if (num == null) {
			if (other.num != null)
				return false;
		} else if (!num.equals(other.num))
			return false;
		return true;
	}

	public void cleanTree() {
		int min = Integer.MAX_VALUE;
		int bestDepth = 0;
		List<TreeId> copyList = new ArrayList<>();
		
		//Search the biggest depth with the minimun id
		for(TreeId tree: childs) {
			if(tree.depth >= bestDepth && tree.getData() < min) {
				min = tree.getData();
				bestDepth = tree.depth;
			}
			copyList.add(tree);
		}
		
		for(TreeId tree: copyList) {
			if(tree.depth <= bestDepth && tree.getData() > min) {
				childs.remove(tree);
			} else if (tree.depth != 0){ 
				tree.cleanTree();
			}
		}
		
	}
}