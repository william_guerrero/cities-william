package com.eurovision.cities.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.eurovision.cities.entity.City;
import com.eurovision.cities.service.CityService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CityController.class)
@WebAppConfiguration
@AutoConfigureMockMvc
public class CityControllerTest {

    @Autowired
    private MockMvc mockMvc;
	
	@Autowired
	private CityController controller;

	@MockBean
	private CityService service;
	
	private List<City> elements = new ArrayList<>();
	
	@Before
	public void prepareData() {
		
		City madrid = new City();
		madrid.setId(1);
		madrid.setName("Madrid");
		
		elements.add(madrid);
		
		City barcelona = new City();
		barcelona.setId(2);
		barcelona.setName("Barcelona");
		
		elements.add(barcelona);
		
		City valencia = new City();
		valencia.setId(3);
		valencia.setName("Valencia");
		
		elements.add(valencia);
	}

	//TODO fix this test
	@Test
	public void testQueryByPage() throws Exception {
		PageRequest pageRequest = PageRequest.of(0,5);
		Page<City> page = new PageImpl<>(elements, pageRequest, 3);
		
		when(service.getCities(0, 5)).thenReturn(page);
		mockMvc.perform(get("/api/cities/queryByPage?page={page}&size={size}", 0, 5))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

}
