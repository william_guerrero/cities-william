package com.eurovision.cities.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.eurovision.cities.entity.City;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CityRepositoryTest {

	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	TestEntityManager entityManager;
	
	@Test
	public void test() {
		City city = new City();
		city.setName("Madrid");
		city = entityManager.persistAndFlush(city);
		assertThat(cityRepository.findById(city.getId()).get()).isEqualTo(city);
	}

}
