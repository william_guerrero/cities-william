package com.eurovision.cities.service;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.eurovision.cities.entity.City;
import com.eurovision.cities.repository.CityRepository;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceImplTest {

	@InjectMocks
	private CityServiceImpl service;

	@Mock
	private CityRepository repository;

	private List<City> elements = new ArrayList<>();
	
	@Before
	public void prepareData() {
		MockitoAnnotations.initMocks(this);
		City madrid = new City();
		madrid.setId(1);
		madrid.setName("Madrid");
		
		elements.add(madrid);
		
		City barcelona = new City();
		barcelona.setId(2);
		barcelona.setName("Barcelona");
		
		elements.add(barcelona);
		
		City valencia = new City();
		valencia.setId(3);
		valencia.setName("Valencia");
		
		elements.add(valencia);
	}
	
	//Base case
	@Test
	public void testGetCities() {
		PageRequest pageRequest = PageRequest.of(0,5);
		Page<City> page = new PageImpl<>(elements, pageRequest, 3);
		when(repository.findAll(any(Pageable.class))).thenReturn(page);
		
		assertThat(service.getCities(0, 5)).isNotNull();
	}
	
}
